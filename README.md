# Windows Server Advanced Power Management - LocalPrintersOnline plugin
LocalPrintersOnline is a plugin for Windows Server Advanced Power Management (https://codeberg.org/DecaTec/Wsapm). It enables you to suppress standby suppress standby if a local printer is switched on

In order to use this plugin, you need the software Windows Server Advanced Power Management (WSAPM):
- [WSAPM homepage](https://decatec.de/software/windows-server-advanced-power-management_en/)
- [WSAPM @ Codeberg](https://codeberg.org/DecaTec/Wsapm)
